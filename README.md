# cmsapp - Client Management System

### This is a sample application that allows to manage clients

This application implements these functionalities:

- Add `New Client`
- Edit a client
- Delete from the table row
- Delete from the edit Screen

# Installation

### Clone the app using:
```
git clone https://gitlab.com/srikantgudi/vcmsapp.git cmsapptest
```

- `cmsapptest` can be replaced by any name of your choice

- if not provided, then `vcmsapp` name will be used by default. Make sure the folder does not exist!

### change to the application folder

> cd vcmsapp

## Install dependencies

> `yarn` 

or

> `npm install`

# Run the app

using either of `yarn serve` | `npm run serve`
